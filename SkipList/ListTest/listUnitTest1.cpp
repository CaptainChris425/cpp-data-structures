#include "stdafx.h"
#include "CppUnitTest.h"
#include "../LinkedList/LList.h"
#include <stdlib.h>
#include <crtdbg.h>
#include <stdexcept>
#include <functional>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

//namespace ListTest
//{		
//	TEST_CLASS(UnitTest1)
//	{
//	public:
//		
//
//		TEST_METHOD(TestInitialSize)
//		{
//			LList<int> list;
//			Assert::AreEqual(0, list.length(), L"Test initial Size ", LINE_INFO());
//		}
//
//		void static functionTest()
//		{
//			LList<int>  list;
//			int x = list.getValue();
//		}
//
//		TEST_METHOD(GetCurrOnEmptyList)
//		{
//			Assert::ExpectException<out_of_range>(&functionTest, L"Test Current on Empty List", LINE_INFO());
//		}
//
//		TEST_METHOD(TestHasNextTrue)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{			
//				list.append(x);
//			}
//			list.moveToStart();
//			for (int x = 0; x < 98; x++)
//			{				
//				list.next();
//				Assert::AreEqual(true, list.hasNext(), L"Error " , LINE_INFO());
//			}
//			
//		}
//		TEST_METHOD(TestHasNextEmptyList)
//		{
//			LList<int> list;
//			Assert::AreEqual(false, list.hasNext(), L"message", LINE_INFO());
//		}
//		TEST_METHOD(TestHasNextFalse)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.moveToEnd();
//
//			Assert::AreEqual(false, list.hasNext(), L"message", LINE_INFO());
//		}
//
//		TEST_METHOD(TestLengthAfterAppend)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				Assert::AreEqual(x, list.length(), L"message", LINE_INFO());
//				list.append(x);
//			}
//
//		}
//
//		TEST_METHOD(TestRemoveHead)
//		{
//			LList<int> list;
//			list.append(1);
//			list.append(2);
//			Assert::AreEqual(1, list.remove(), L"Remove does not return correct value", LINE_INFO());
//			Assert::AreEqual(1, list.length(), L"Length Not Decreasing on Remove", LINE_INFO());
//			Assert::AreEqual(2, list.getValue(), L"Current Not Changing on Remove", LINE_INFO());
//		}
//
//		TEST_METHOD(TestRemoveTail)
//		{
//			LList<int> list;
//			list.append(1);
//			list.append(2);
//			list.append(3);
//			list.moveToEnd();
//			list.remove();
//			Assert::AreEqual(2, list.length(), L"Length Not Decreasing on Remove", LINE_INFO());
//			Assert::AreEqual(2, list.getValue(), L"Current Not Changing on Remove", LINE_INFO());
//
//
//		}
//		void static functionTest2(LList<int>  list)
//		{
//			int x = list.getValue();
//		}
//
//		TEST_METHOD(TestRemoveLast)
//		{
//			LList<int> list;
//			list.append(1);
//			list.append(2);
//			list.append(3);
//			list.moveToEnd();
//			Assert::AreEqual(3, list.remove(), L"Remove does not return correct value", LINE_INFO());
//			Assert::AreEqual(2, list.length(), L"Length Not Decreasing on Last Remove", LINE_INFO());
//			Assert::AreEqual(2, list.remove(), L"Remove does not return correct value", LINE_INFO());
//			Assert::AreEqual(1, list.length(), L"Length Not Decreasing on Last Remove", LINE_INFO());
//			Assert::AreEqual(1, list.getValue(), L"Current Not Null on Last Remove", LINE_INFO());
//			Assert::AreEqual(1, list.remove(), L"Remove does not return correct value", LINE_INFO());
//			Assert::AreEqual(0, list.length(), L"Length Not Decreasing on Last Remove", LINE_INFO());
//
//		}
//
//		TEST_METHOD(TestLengthAfterRemove)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				Assert::AreEqual(x, list.length(), L"Length Not Increasing", LINE_INFO());
//				list.append(x);
//			}
//			for (int x = 99; x > 0; x--)
//			{
//				list.remove();
//				Assert::AreEqual(x, list.length(), L"Length Not Decreasing on Remove", LINE_INFO());
//
//			}
//
//		}
//		
//
//		TEST_METHOD(TestHeadAfterAppend)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//
//				list.append(x);
//			}
//			list.moveToStart();
//
//			Assert::AreEqual(0, list.getValue(), L"Head Element not 0 Error", LINE_INFO());
//
//
//		}
//		TEST_METHOD(TestTaildAfterAppend)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//
//				list.append(x);
//			}
//			list.moveToEnd();
//
//			Assert::AreEqual(99, list.getValue(), L"Head Element not 0 Error", LINE_INFO());
//
//
//		}
//
//		TEST_METHOD(TestInsertEmptyList)
//		{
//			LList<int> list;
//			list.insert(1);
//			int x = list.getValue();
//			Assert::AreEqual(1, x, L"Head Element not 0 Error", LINE_INFO());
//
//
//		}
//
//		TEST_METHOD(TestInsertAtTail)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.moveToEnd();
//			list.insert(111);
//			list.moveToEnd();
//			list.prev();
//			Assert::AreEqual(111, list.getValue(), L"Head Element not 0 Error", LINE_INFO());
//
//
//		}
//
//		TEST_METHOD(TestPrepend)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.moveToStart();
//			list.prepend(111);
//			list.moveToStart();
//			Assert::AreEqual(111, list.getValue(), L"Head Element not 0 Error", LINE_INFO());
//
//
//		}
//
//		TEST_METHOD(TestInsertInMiddle)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.next();
//			list.next();
//			list.next();
//			list.insert(111);
//			list.prev();
//			Assert::AreEqual(111, list.getValue(), L"Wrong value", LINE_INFO());
//
//
//		}
//
//		TEST_METHOD(TestNext)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//
//				list.append(x);
//			}
//			for (int x = 0; x < 100; x++)
//			{
//				Assert::AreEqual(x, list.getValue(), L"Next Element not correct Error", LINE_INFO());
//				list.next();
//			}
//
//		}
//
//
//		TEST_METHOD(TestPrevious)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.moveToEnd();
//			for (int x = 99; x >= 0; x--)
//			{
//				
//				Assert::AreEqual(x, list.getValue(), L"Next Element not correct Error", LINE_INFO());
//				list.prev();
//			}
//
//		}
//
//
//
//		TEST_METHOD(TestPreviousNextCombo)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.moveToEnd();
//			for (int x = 99; x >= 50; x--)
//			{
//
//				Assert::AreEqual(x, list.getValue(), L"Next Element not correct Error", LINE_INFO());
//				list.prev();
//			}
//
//			for (int x = 49; x >= 100; x--)
//			{
//
//				Assert::AreEqual(x, list.getValue(), L"Next Element not correct Error", LINE_INFO());
//				list.next();
//			}
//
//		}
//
//		TEST_METHOD(TestGotoPos)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			//list.moveToEnd();
//			for (int y = 0; y < 100; y++)
//			{
//				list.moveToPos(y);
//				Assert::AreEqual(y, list.getValue(), L"Next Element not correct Error", LINE_INFO());
//
//			}
//
//		}
//
//		TEST_METHOD(TestCurrentPos)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}			
//			for (int y = 0; y < 100; y++)
//			{
//				list.moveToPos(y);
//				Assert::AreEqual(y, list.currPos() , L"Next Element not correct Error", LINE_INFO());
//
//			}
//
//		}
//		TEST_METHOD(TestClear)
//		{
//			LList<int> list;
//			for (int x = 0; x < 100; x++)
//			{
//				list.append(x);
//			}
//			list.clear();
//			Assert::AreEqual(0, list.length(), L"Next Element not correct Error", LINE_INFO());
//
//		}
//
//	};
//}