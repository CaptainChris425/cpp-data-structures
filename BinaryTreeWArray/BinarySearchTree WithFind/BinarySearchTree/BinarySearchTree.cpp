// BinarySearchTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BSTree.h"
#include "ComputerScientist.h"

int _tmain(int argc, _TCHAR* argv[])
{
	ComputerScientist::sortBy = SortBy::id;
	BinarySearchTree<ComputerScientist> tree;
	tree.insert(ComputerScientist("Robert", "Ward", "Education", 50));
	tree.insert(ComputerScientist("Grace", "Hopper", "Compilers", 40));
	tree.insert(ComputerScientist("Alan", "Turing", "Cryptogarphy", 60));
	tree.insert(ComputerScientist("John", "von Neumann", "Hardware", 45));
	tree.insert(ComputerScientist("Dennis", "Ritchie", "Compilers", 55));
	tree.insert(ComputerScientist("Ada", "Lovelace", "Programming", 30));
	tree.insert(ComputerScientist("Edsger", "Dikstra", "Algorithms", 80));
	tree.insert(ComputerScientist("Ken", "Thompson", "OS", 70));

	tree.printTree();
	cout << endl;
	ComputerScientist find(80);
		
	ComputerScientist find2 = tree.find(find);

	tree.remove(45);
	tree.printTree();
	cout << endl;
	tree.makeEmpty();
	tree.printTree();
		
	system("pause");
	return 0;
}

