#include "stdafx.h"
#include "ComputerScientist.h"


SortBy ComputerScientist::sortBy = SortBy::id;
ostream& operator<< (ostream &out, const ComputerScientist &ComputerScientist)
{
	// Since operator<< is a friend of the Point class, we can access
	// Point's members directly.
	out << "(" << ComputerScientist.firstName << " " << ComputerScientist.lastName;
	out << ", id: " << ComputerScientist.id;
	out << ", speciality: " << ComputerScientist.speciality;
	out << ")";
	return out;
}

ComputerScientist::ComputerScientist(const ComputerScientist& rhs)
{
	this->firstName = rhs.firstName;
	this->lastName = rhs.lastName;
	this->speciality = rhs.speciality;
	this->id = rhs.id;

}
ComputerScientist::ComputerScientist(string firstName, string lastName, string speciality, int id)
{
	this->firstName = firstName;
	this->lastName = lastName;
	this->speciality = speciality;
	this->id = id;
}
ComputerScientist::ComputerScientist(string firstName, string lastName)
{
	this->firstName = firstName;
	this->lastName = lastName;
	this->speciality = "";
	this->id = 0;
}
ComputerScientist::ComputerScientist(string speciality)
{
	this->firstName = "";
	this->lastName = "";
	this->id = 0;
	this->speciality = speciality;
	
}
ComputerScientist::ComputerScientist(int id)
{
	this->firstName = "";
	this->lastName = "";
	this->speciality = "";
	this->id = id;
}
ComputerScientist::ComputerScientist( )
{


}

ComputerScientist::~ComputerScientist()
{
}
bool ComputerScientist::operator==( const ComputerScientist& rhs) const
{
	switch (ComputerScientist::sortBy)
	{
		case SortBy::firstName:
			return this->firstName == rhs.firstName;
			break;
		case SortBy::lastName:
			return this->lastName == rhs.lastName;
			break;
		case SortBy::speciality:
			return this->speciality == rhs.speciality;
			break;
		case SortBy::id:
			return this->id == rhs.id;
			break;
		default:
			return this->firstName == rhs.firstName;
			break;
	}

}
bool  ComputerScientist::operator<( const ComputerScientist& rhs) const
{
	switch (ComputerScientist::sortBy)
	{
	case SortBy::firstName:
		return this->firstName < rhs.firstName;
		break;
	case SortBy::lastName:
		return this->lastName < rhs.lastName;
		break;
	case SortBy::speciality:
		return this->speciality < rhs.speciality;
		break;
	case SortBy::id:
		return this->id < rhs.id;
		break;
	default:
		return this->firstName < rhs.firstName;
		break;
	}
}

string ComputerScientist::getFirstName()
{
	return this->firstName;
}
string ComputerScientist::getLastName()
{
	return this->lastName;
}
string ComputerScientist::getSpeciality()
{
	return this->speciality;
}
int ComputerScientist::getID()
{
	return this->id;
}