#include "stdafx.h"
#include "HuffmanTreeInterface.h"
#include <string>
#include <vector>

using namespace std;
class HuffmanTree : HuffmanTreeInterface
{
private:
	class BinaryNode
	{
	public:
		string element;
		int frequency;
		BinaryNode *left;
		BinaryNode *right;
		//Initialize class members from constructor arguments 
		//by using a member initializer list.
		//This method uses direct initialization, which is more
		//efficient than using assignment operators inside the constructor body.
		BinaryNode(const string & theElement, int frequency, BinaryNode *left = NULL, BinaryNode *right = NULL)
			: element( theElement ), frequency( frequency ), left( left ), right( right)
		{

		}

	};
	BinaryNode *root = NULL;
	bool HuffmanTree::hasLetter(string word, char letter);
	void makeEmpty(BinaryNode * & t);
	
	void printTree(BinaryNode *t, std::ostream & out) const;

	void printCodes(BinaryNode *t, std::ostream & out, string code) const;

	static bool comparefunction(BinaryNode * i, BinaryNode * j);
	
	BinaryNode * buildTree(vector<BinaryNode *> nodes);

public:
	
	HuffmanTree(string frequencyText);
	~HuffmanTree();
	void printTree(std::ostream & out = cout) const;
	void printCodes(std::ostream & out = cout) const;
	void makeEmpty();

	string encode(string stringToEncode);
	string decode(string stringToDecode);
	
	void saveEncode(string stringToEncode, string fileName);
	void saveDecode(string stringToDecode, string fileName);
	
	void uncompressFile(string compressedFileName, string uncompressedFileName);

	
	



};