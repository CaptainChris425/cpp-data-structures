#include "stdafx.h"
#include "HuffmanTree.h"
#include <string>
#include <vector>
#include <fstream> 
#include <iostream>
#include <algorithm>
using namespace std;

void HuffmanTree::makeEmpty(BinaryNode * & t)
{
	//post order traversal
	if (t != nullptr) {
		makeEmpty(t->left);
		makeEmpty(t->right);
		delete t;
	}

}
void HuffmanTree::printTree(BinaryNode *t, std::ostream & out) const
{
	// in order traversal
	if (t != nullptr) {
		out << t->element << endl;
		printTree(t->left, out);
		printTree(t->right, out);
	}
}

void HuffmanTree::printCodes(BinaryNode *t, std::ostream & out, string code) const
{
	if (t != nullptr) 
	{
		if (t->left != nullptr && t->right != nullptr) {
				if (t->element != "\n") // checks if character is not the end string
					out << "Character::" << t->element << "\t" << "Encoded:: " << code << endl;
				else
					out << "Character::" << "\\n" << "\t" << "Encoded:: " << code << endl;
		}
		printCodes(t->left, out, code + '0'); // adds 0 to code for going left
		printCodes(t->right, out, code + '1'); // adds 1 to code for going right

	}
}

bool HuffmanTree::comparefunction(BinaryNode * i, BinaryNode * j)
{
	// true if first frequency is less than 2nd frequency
	if (i->frequency >= j->frequency)
		return false;
	return true;
}

HuffmanTree::BinaryNode * HuffmanTree::buildTree(vector<BinaryNode *> nodes)
{
	while (nodes.size() > 1)
	{
		std::sort(nodes.begin(), nodes.end(), comparefunction); // sort tree
		BinaryNode * first = nodes[0]; //save first two nodes
		BinaryNode * second = nodes[1];
		nodes.erase(nodes.begin()); //delete first two nodes
		nodes.erase(nodes.begin());
		nodes.push_back(new BinaryNode(first->element + second->element, first->frequency + second->frequency, first, second));
	}
	return nodes[0]; //root node

}
HuffmanTree::HuffmanTree(string frequencyText)
{
	vector<BinaryNode *> nodes;
	int frequencies[256] = { 0 };
	for (char c : frequencyText)
		frequencies[c]++;
	for (int i = 0; i < 256; i++) //for every possible char in ASCII
	{
		string mystring; //make a new string that has that char in it
		mystring.push_back(i);
		if (frequencies[i] != 0) // if char appears in string
			nodes.push_back(new BinaryNode(mystring, frequencies[i])); // adds new node with string and frequency
	}
	root = buildTree(nodes);

}

HuffmanTree::~HuffmanTree()
{
	makeEmpty(root);
};
void HuffmanTree::printCodes(std::ostream & out) const
{
	printCodes(root, out, "");
}


void HuffmanTree::printTree(std::ostream & out) const
{
	printTree(root, out);
}

void HuffmanTree::makeEmpty()
{
	makeEmpty(root);
}
string HuffmanTree::decode(string encodedString)
{
	string decoded = "";
	int size = encodedString.length();
	BinaryNode * temp = root;
	for (int i = 0; i < size; i++) {
		if (encodedString[i] == '0')
			temp = temp->left; // if the char in the encoded is 1 go right
		else
			temp = temp->right; // if the char in the encoded is 0 go left
		if (temp->left == nullptr) {
			decoded += temp->element[0];
			temp = root;
		}
	}
	return decoded;
}

void HuffmanTree::saveDecode(string encodedString, string fileName)
{
	
	ofstream savefile;
	savefile.open(fileName, ios::out | ios::binary);
	int size = encodedString.length();
	BinaryNode* temp = root;
	for (int i = 0; i < size; i++) {
		if (encodedString[i] == '0')
			temp = temp->left; // go left if found a 0
		else
			temp = temp->right; // go right if found a 1
		if (temp->element.size() == 1){
			savefile << temp->element[0];
			temp = root;
		}
	}
	savefile.close();

}
string HuffmanTree::encode(string stringToEncode)
{
	string encoded = "";
	int size = stringToEncode.length();
	BinaryNode * temp = root;
	for (int i = 0; i < size; i++) {
		while (temp->element.length() > 1) {
			if (hasLetter(temp->left->element, stringToEncode[i])) {
				// go left and add a 0
				encoded += '0';
				temp = temp->left;
			}
			else{
				// go right and add a 1
				encoded += '1';
				temp = temp->right;
			}
		}
		temp = root;
	}
	
	return encoded;
}

bool HuffmanTree::hasLetter(string word, char letter) {
	for (char temp : word) {
		if (temp == letter)
			return true;
	}
	return false;
}
void HuffmanTree::saveEncode(string stringToEncode, string fileName)
{
	stringToEncode = encode(stringToEncode);
	int size = stringToEncode.length(); // length after turning binary
	ofstream savefile;
	savefile.open(fileName, ios::out | ios::binary);
	char byte = 0;
	unsigned int i;
	for (i = 0; i < size; i++) {
		
		if (stringToEncode[i] == '1') {
			byte |= 1 << (i%8);
		}
		if (((i + 1) % 8) == 0 && i > 0) {
			savefile << byte;
			byte = 0;
		}
	}

	while (i % 8 != 0) {
		byte &= ~(1 << (i % 8));
	}
	savefile << byte;
	savefile.close();
}


void HuffmanTree::uncompressFile(string compressedFileName, string uncompressedFileName)
{
	
}
