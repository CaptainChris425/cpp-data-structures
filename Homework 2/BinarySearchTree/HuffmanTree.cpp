#include "stdafx.h"
#include "HuffmanTree.h"
#include <string>
#include <vector>
#include <fstream> 
#include <iostream>
#include <algorithm>
using namespace std;

void HuffmanTree::makeEmpty(BinaryNode * & t)
{
	if (t)
	{
		makeEmpty(t->left);
		makeEmpty(t->right);
		delete t;
	}
}
void HuffmanTree::printTree(BinaryNode *t, std::ostream & out) const
{
	if (t)
	{
		out << t->element << endl;
		printTree(t->left, out);	
		printTree(t->right, out);
	}
}

void HuffmanTree::printCodes(BinaryNode *t, std::ostream & out, string code ) const
{
	if (t)
	{
		if (!t->left && !t->right)
		{
			if (t->element != "\n")
				out << "char: " << t->element << "\tCode: " << code << endl;
			else
				out << "char:\\n" << "\tCode: " << code << endl;
		}
		printCodes(t->left, out, code + '0');
		printCodes(t->right, out, code + '1');
	}
}

bool HuffmanTree::compareFunction(BinaryNode * i, BinaryNode * j)
{
	if (i->frequency >= j->frequency)
		return false;
	return true;
}

HuffmanTree::BinaryNode * HuffmanTree::buildTree(vector<BinaryNode *> nodes)
{
	while (nodes.size() > 1)
	{
		std::sort(nodes.begin(), nodes.end(), compareFunction); // sort tree
		BinaryNode * first = nodes[0]; //save first two nodes
		BinaryNode * second = nodes[1];
		nodes.erase(nodes.begin()); //delete first two nodes
		nodes.erase(nodes.begin());
		nodes.push_back(new BinaryNode(first->element + second->element, first->frequency + second->frequency, first, second));
	}
	return nodes[0]; //return root node
}
HuffmanTree::HuffmanTree(string frequencyText)
{
	vector<BinaryNode *> nodes;
	int frequencies[256] = { 0 };
	for (char c : frequencyText)
		frequencies[c]++;
	for (int i = 0; i < 256; i++) //for every possible char in ASCII
	{
		string mystring; //make a new string that has that char in it
		mystring.push_back(i);
		if (frequencies[i] != 0) // if char appears in string
			nodes.push_back(new BinaryNode(mystring, frequencies[i])); // adds new node with string and frequency
	}
	root = buildTree(nodes);
}

HuffmanTree::~HuffmanTree()
{
	makeEmpty();
};

void HuffmanTree::printCodes(std::ostream & out) const
{
	printCodes(root, out, "");
}

void HuffmanTree::printTree(std::ostream & out) const
{
	printTree(root, out);
}

void HuffmanTree::makeEmpty()
{
	makeEmpty(root);
}

string HuffmanTree::decode(string encodedString)
{
	string decoded = "";
	int length = encodedString.length();
	BinaryNode * t = root;
	for (int i = 0; i < length; i++)
	{
		if (encodedString[i] == '0') //if char is in the left tree
			t = t->left;
		else
			t = t->right;

		if (!t->left) //until it finds that char
		{
			decoded += t->element[0];
			t = root; //resets t back to root
		}
	}
	return decoded;
}

void HuffmanTree::saveDecode(string encodedString, string fileName)
{
	ofstream save;
	save.open(fileName, ios::out | ios::binary); //opens file
	int length = encodedString.length();
	BinaryNode * t = root;
	for (int i = 0; i < length; i++)
	{
		if (encodedString[i] == '0') //if char is in the left tree
			t = t->left;
		else
			t = t->right;

		if (t->element.size() == 1) //until it finds that char
		{
			save << t->element[0];
			t = root; //resets t back to root
		}
	}
	save.close();
}

//returns a string of 1's and 0's containing all the compressed data according to the huffman tree
string HuffmanTree::encode(string stringToEncode)
{
	string encoded = "";
	int lengthOfString = stringToEncode.length();
	BinaryNode * t = root;
	for (int i = 0; i < lengthOfString; i++)
	{
		while (t->element.length() > 1) //until it finds that char
		{
			if (contains(t->left->element, stringToEncode[i])) //if char is in the left tree
			{
				encoded += '0'; //go left and add a 0
				t = t->left;
			}
			else
			{
				encoded += '1'; //go right and add a 1
				t = t->right;
			}
		}  
		t = root; //resets t back to root
	}
	
	return encoded;
}

void HuffmanTree::saveEncode(string stringToEncode, string fileName)
{
	stringToEncode = encode(stringToEncode); //turns into binary
	int lengthOfString = stringToEncode.length();
	ofstream save;
	save.open(fileName, ios::out | ios::binary);//opens file

	char byte = 0;
	unsigned int i;
	for (i = 0; i < lengthOfString; i++)
	{
		char bit = stringToEncode[i]; //makes it either a 1 or a 0

		// Add a single bit to byte
		if (bit == '1')
			byte |= (1 << (7 - (i % 8))); //sets bits from left to right

		if (((i+1) % 8) == 0 && i > 0) //when byte is full
		{
			//cout << ' ';
			save << byte;
			byte = 0;
		}
	}

	while (i % 8 != 0)	//Fill the last incomplete byte if there is one
		byte &= ~(1 << (7 - (i++ % 8))); //adds a 0 to current position in the byte
	save << byte;

	save.close();
}
void HuffmanTree::uncompressFile(string compressedFileName, string uncompressedFileName)
{
	
}

bool HuffmanTree::contains(string search, char key) const
{
	for (char c : search)
		if (c == key)
			return true;
	return false;
}
