#include "BSTInterface.h"

template <typename Comparable>
class BinarySearchTree : BSTInterface < Comparable >
{
private:
	class BinaryNode
	{
	public:
		Comparable element;
		BinaryNode *left;
		BinaryNode *right;
		int height = 0;
		void setHeight(int height) {
			if (this->height < height) {
				this->height = height;
			}
		}
		int balanceFactor() {
			int leftHeight = this->left == nullptr ? -1 : this->left->height;
			int rightHeight = this->right == nullptr ? -1 : this->right->height;
			return (leftHeight - rightHeight);
		}
		//Initialize class members from constructor arguments 
		//by using a member initializer list.
		//This method uses direct initialization, which is more
		//efficient than using assignment operators inside the constructor body.
		BinaryNode(const Comparable & theElement, BinaryNode *left = nullptr, BinaryNode *right = nullptr)
			: element{ theElement }, left{ left }, right{ right }
		{

		}
	};
	BinaryNode *root = nullptr;
	int insert(const Comparable & item, BinaryNode ** t)
	{
		if ((*t) == nullptr) {
			(*t) = new BinaryNode(item);
		}
		else if (item < (*t)->element) {
			(*t)->setHeight(insert(item, &((*t)->left)) + 1);
		}
		else {
			(*t)->setHeight(insert(item, &((*t)->right)) + 1);
		}
		if ((*t)->balanceFactor() > 1) {
			balance(t, true);
		}
		else if ((*t)->balanceFactor() < -1) {
			balance(t, false);
		}
		return (*t)->height;



	}


	BinaryNode * removeNode(BinaryNode * t)
	{
		// set for one child
		BinaryNode * newChildNode = nullptr;
		//two children
		if (t->left != nullptr && t->right != nullptr)
		{
			// we won't change the node
			newChildNode = t;
			t->element = findMin(t->right)->element;
			remove(t->element, t->right);
			// do not delete t 
			// it is just updted in this case
			return newChildNode;
		}
		//one child (can't be both previos condtion)
		// if both are nullptr zero children then the are equal
		else if (t->left != t->right)
		{
			newChildNode = t->left == nullptr ? t->right : t->left;
		}
		//delete this node
		delete t;
		// return this to update the parent
		return newChildNode;
	}
	void remove(const Comparable & item, BinaryNode ** t)
	{
		if (t == nullptr)
		{
			return;
		}
		if (item == ((*t)->element)) {
			if ((*t)->left == nullptr && (*t)->left == nullptr) {
				delete (*t);
				(*t) = nullptr;
			}
			else if ((*t)->left == nullptr) {
				delete (*t);
				(*t) = (*t)->right;
			}
			else if ((*t)->right == nullptr) {
				delete (*t);
				(*t) = (*t)->left;
			}
			else {
				(*t)->element = findMin(((*t)->right))->element;
				remove((*t)->element, &((*t)->right));
			}
		}
	}
	BinaryNode * findMin(BinaryNode *t) const
	{
		while (t->left != nullptr)
		{
			t = t->left;
		}
		return t;
	}
	BinaryNode * findMax(BinaryNode *t) const
	{
		while (t->right != nullptr)
		{
			t = t->right;
		}
		return t;
	}
	BinaryNode *  find(const Comparable & item, BinaryNode *t) const
	{
		if (t == nullptr)
		{
			return nullptr;
		}
		if (t->element == item)
		{
			return t;
		}
		if (item < t->element)
		{
			return find(item, t->left);
		}
		else {
			return find(item, t->right);
		}
		return find(item, t->left);


	}
	bool contains(const Comparable & item, BinaryNode *t) const
	{
		if (find(item, t) == nullptr)
		{
			return false;
		}
		return true;

	}
	void makeEmpty(BinaryNode * & t)
	{
		if (t != nullptr)
		{
			makeEmpty(t->left);
			makeEmpty(t->right);
			delete t;
		}

	}
	void printTree(BinaryNode *t, std::ostream & out) const
	{
		if (t == nullptr) {
			return;
		}
		printTree(t->left, out);
		out << "Height: " << t->height;
		out << "Balance Factor: " << t->balanceFactor() << endl;
		out << t->element << endl;
		printTree(t->right, out);

	}

	void balance(BinaryNode **t, bool leftCase) {
		if (leftCase) {
			if ((*t)->left->balanceFactor() == -1) {
				BinaryNode* temp = (*t);
				temp = (*t)->left;
				(*t)->left = temp->right;
				temp->right = (*t)->left->left;
				(*t)->left->left = temp;
				temp = (*t)->left;
				(*t)->left = temp->right;
				temp->right = (*t);
				(*t) = temp;
				resetHeights(*t);
				cout << "left right" << endl;
			}
			if ((*t)->left->balanceFactor() == 1) {
				BinaryNode * temp = (*t);
				(*t) = temp->left;
				temp->left = (*t)->left;
				(*t)->right = temp;
				resetHeights(*t);
				cout << "left left" << endl;
			}
		}
		else {
			if ((*t)->right->balanceFactor() == 1) {
				BinaryNode * temp = (*t);
				temp = (*t)->right;
				(*t)->right = (*t)->right->left->left;
				temp->left->left = (*t);
				(*t) = temp->left;
				temp->left = (*t)->right;
				(*t)->right = temp;
				resetHeights(*t);
				cout << "right left" << endl;
			}
			if ((*t)->right->balanceFactor() == -1) {
				BinaryNode * temp = (*t);
				(*t) = temp->right;
				temp->right = (*t)->right;
				(*t)->left = temp;
				resetHeights(*t);
				cout << "right right" << endl;
			}
		}
	}

	int resetHeights(BinaryNode* t) {
		if (t == nullptr) {
			int a = -1;
			return a;
		}
		int left = resetHeights(t->left);
		int right = resetHeights(t->right);
		t->height = left > right ? left + 1 : right + 1;
		return t->height;
	}

public:
	BinarySearchTree()
	{
		root = nullptr;
	};

	~BinarySearchTree()
	{
		makeEmpty();
	};

	const Comparable & findMin() const
	{
		//should throw exception on empty tree
		return findMin(root)->element;
	};
	const Comparable & findMax() const
	{
		//should throw exception on empty tree
		return findMax(root)->element;
	};

	const Comparable & find(const Comparable & item) const
	{
		BinaryNode * found = find(item, root);
		//should throw exception on nullptr
		return found->element;
	};

	bool contains(const Comparable & item) const
	{
		return contains(item, root);
	};
	bool isEmpty() const {

		return root == nullptr;
	};
	void printTree(std::ostream & out = cout) const
	{
		printTree(root, out);
	};

	void makeEmpty()
	{
		makeEmpty(root);
	};
	void insert(const Comparable & item)
	{
		if (root == nullptr)
		{
			root = new BinaryNode(item);
		}
		else
		{
			insert(item, &root);
		}

	};

	void remove(const Comparable & item)
	{
		remove(item, &root);

	};




};