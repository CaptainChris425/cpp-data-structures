// BinarySearchTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HuffmanTree.h"
#include <string>
#include <fstream>
#include <streambuf>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{

	std::ifstream t("20000leagues.txt");
	std::string leagues((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
	HuffmanTree tree(leagues);

	tree.printCodes();

	std::ifstream bigtext("20000leagues.txt");
	std::string big((std::istreambuf_iterator<char>(bigtext)), std::istreambuf_iterator<char>());

	string decodeWords = tree.encode(big);

	tree.saveEncode(big, "binary.bin");
	tree.saveDecode(decodeWords, "Decode.txt");

	cout << "\n\n" << tree.decode(decodeWords) << "\n\n\n\n\n\n\n"; //output decoded version of encoded file
	

	system("pause");
	return 0;
}

