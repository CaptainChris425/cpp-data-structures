// BinarySearchTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HuffmanTree.h"
#include <string>
#include <fstream>
#include <streambuf>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	//char save = 0;
	//for (int i = 0; i < 7;i++)
	//{
	//	save |= 1 << i;
	//}
	////save |= 1 << 7;
	////int bit = 6;
	////save |= 1 << bit;
	//cout << (int)save;
	//cout << (int)(save | 1 << 7);

	std::ifstream t("20000leagues.txt");
	std::string leagues((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
	HuffmanTree tree(leagues);
	
	tree.printCodes();

	std::ifstream bigtext("20000leagues.txt");
	std::string big((std::istreambuf_iterator<char>(bigtext)), std::istreambuf_iterator<char>());
		
	string decodeThing = tree.encode(big);

	tree.saveEncode(big, "binary.bin");
	tree.saveDecode(decodeThing, "Decode.txt");

	cout << "\n\n" << tree.decode(decodeThing) << "\n\n\n\n\n\n\n"; //output decoded version of encoded file
	//cout << decodeThing << endl; //output binary encoding

	system("pause");
	return 0;
}

