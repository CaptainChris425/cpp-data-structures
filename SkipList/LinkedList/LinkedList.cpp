// LinkedList.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdexcept>
#include <iostream>
#include "LList.h"
#include <string>
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
		

	LList<int> list;
	for (int i = 10; i < 30; i++) {
		list.insert(i);
		list.print();
	}
	list.insert(1);
	list.print();
	list.insert(50);
	list.insert(49);
	list.insert(37);
	cout << endl << "find" << endl;
	if (list.find(9)) {
		cout << "found 9" << endl;
	}
	list.insert(50);
	list.print(cout);
	cout << endl << endl;
	list.remove(12);
	cout << endl << endl << "List without mid node(12)" << endl;
	list.print(cout);
	list.remove(13);
	cout << endl << endl << "List without mid node(13)" << endl;
	list.print(cout);
	cout << endl << endl << "List without first node(11)" << endl;
	list.remove(11);
	list.print(cout);
	cout << endl << endl << "List without last node(20)" << endl;
	cout << endl << "remove" << endl;
	list.remove(20);
	list.print(cout);
	cout << endl << "finding 16";
	if (list.find(16)) {
		cout << endl << "found 16";
	}
	system("pause");

	return 0; 
}

