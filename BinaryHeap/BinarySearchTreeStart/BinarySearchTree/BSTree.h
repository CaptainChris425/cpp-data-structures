#include "BSTInterface.h"

template <typename Comparable>
class BinarySearchTree : BSTInterface < Comparable >
{
private:
	// the root node of the tree

	/*
	* Private Node Class 
	* 
	*
	*/
	class BinaryNode
	{
		public:
			Comparable element;
			BinaryNode *left;
			BinaryNode *right;
			int index;
			//Initialize class members from constructor arguments 
			//by using a member initializer list.
			//This method uses direct initialization, which is more
			//efficient than using assignment operators inside the constructor body.
			BinaryNode(const Comparable & theElement, BinaryNode *left = nullptr, BinaryNode *right = nullptr)
				: element{ theElement }, left{ left }, right{ right }
			{

			}

	};

	BinaryNode *root = nullptr;
	/*
	* Inserts a node into the tree
	* maintains this property of th tree:
	*     All parent nodes will be greater
	*     All child nodes will be less
	*
	*/
	void insert(const Comparable & item, BinaryNode ** t)
	{
		if ((*t) == nullptr) {
			(*t) = new BinaryNode(item);
		}
		else if (item < (*t)->element) {
			if ((*t)->left == nullptr) {
				(*t)->left = new BinaryNode(item);
			}
			else if ((*t)->right == nullptr) {
				(*t)->right = new BinaryNode(item);
			}
			else {
				if (item < (*t)->left->element) {
					insert(item, &(*t)->left);
				}
				else if (item < (*t)->right->element) {
					insert(item, &(*t)->right);
				}
				else{
					insert(item, &(*t)->left);
				}
			}
		}else if((*t)->element == item){
		}else{
			Comparable temp = (*t)->element;
			(*t)->element = item;
			insert(temp, &(*t));
		}








		//if ((*t) == nullptr) {
		//	(*t) = new BinaryNode(item);
		//}
		//else if (item < (*t)->element) {
		//	if ((*t)->left != nullptr && item < (*t)->left->element) {
		//		insert(item, &(*t)->left);
		//	}
		//	else if ((*t)->right != nullptr && item < (*t)->right->element) {
		//		insert(item, &(*t)->right);
		//	}
		//	else if ((*t)->right == nullptr && (*t)->left == nullptr) {
		//		(*t)->left = new BinaryNode(item);
		//	}
		///*	if ((*t)->right != nullptr && (*t)->left != nullptr) {
		//		if ((*t)->left->left == nullptr) {
		//			(*t)->left->left = new BinaryNode(item);
		//		}else if ((*t)->left->left != nullptr && (*t)->left->right == nullptr) {
		//			(*t)->left->right = new BinaryNode(item);
		//		}
		//		else if ((*t)->left->right != nullptr) {
		//			insert(item, &(*t)->right);
		//		}
		//		
		//	}
		//	else if ((*t)->left != nullptr && (*t)->right == nullptr) {
		//		(*t)->right = new BinaryNode(item);
		//	}
		//	else if ((*t)->left == nullptr && (*t)->right == nullptr) {
		//		(*t)->left = new BinaryNode(item);
		//	}	*/
		//}
		//else {
		//	Comparable temp = (*t)->element;
		//	(*t)->element = item;
		//	insert(temp, &(*t));
		//}
		
	}
	/*
	* remove a node from the tree
	* maintains this property of th tree:
	*     All parent nodes will be greater
	*     All child nodes will be less
	*
	*/
	void remove(const Comparable & item, BinaryNode ** t)
	{
	
	}
	/*
	* Finds the node with the smallest element in the tree
	*
	*/
	BinaryNode * findMin(BinaryNode *t) const
	{

	}
	/*
	* Finds the node with the largest element in the tree
	*
	*/
	BinaryNode * findMax(BinaryNode *t) const
	{

	}
	/*
	* Finds the node with that satisfies equality for the element
	*
	*/
	BinaryNode *  find(const Comparable & item, BinaryNode *t) const
	{
		// stub code remove
		return root;
		
	}
	/*
	* Returns true if the item is found in the tree
	*
	*/
	bool contains(const Comparable & item, BinaryNode *t) const
	{


	}
	/*
	* Removes all elelements from the tree
	*
	*/
	void makeEmpty(BinaryNode * & t)
	{


	}
	/*
	* Prints the inorder the tree to the stream out
	*
	*/
	void printTree(BinaryNode *t, std::ostream & out) const
	{
		
		
	}
	
public:
	BinarySearchTree()
	{
		// add needed code
	};
	
	~BinarySearchTree()
	{
		// add needed code
	};
	/*
	* Finds the node with the smallest element in the tree
	*
	*/
	const Comparable & findMin() const
	{
		// stub code remove
		return Comparable();
	};
	/*
	* Finds the node with the largest element in the tree
	*
	*/
	const Comparable & findMax() const
	{
		// stub code remove
		return Comparable();
	};
	/*
	* Finds the node with that satisfies equality for the element
	* retruns nullptr if it does not 
	*
	*/
	const Comparable & find(const Comparable & item) const
	{
		// stub code remove
		BinaryNode * found = find(item, root);
		return found->element;
	};
	/*
	* Returns true if the item is found in the tree
	*
	*/
	bool contains(const Comparable & item) const
	{
		// stub code remove
		return false;
	};
	/*
	* Returns true if tree has no nodes
	*
	*/
	bool isEmpty() const{
		// stub code remove
		return false;
	};
	/*
	* Prints the inorder the tree to the stream out
	*
	*/
	void printTree(std::ostream & out = cout) const
	{
		
	};
	/*
	* Removes all nodes from the tree
	*
	*/
	void makeEmpty()
	{

	};
	/*
	* Inserts a node into the tree
	* maintains this property of th tree:
	*     All nodes to the left will be less
	*     All nodes to the right will be greater
	*
	*/
	void insert(const Comparable & item)
	{
		// stub code remove
		insert(item, &root);
	};
	/*
	* Removes the nodes if it comtains the given item
	*
	*/
	void remove(const Comparable & item)
	{

	};

	


};