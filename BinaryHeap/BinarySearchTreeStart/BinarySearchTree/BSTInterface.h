
#include <iostream>
#include <ostream>
template <typename Comparable>
 class BSTInterface 
 {
 private:
	
 public:
	 BSTInterface() {};
	 BSTInterface(const BSTInterface & rhs) {};
	 BSTInterface(BSTInterface && rhs) {};
	 virtual ~BSTInterface() {};
		
	virtual const Comparable & findMin() const = 0;
	virtual const Comparable & findMax() const = 0;
	virtual bool contains(const Comparable & item) const = 0;
	virtual const Comparable & find(const Comparable & item) const = 0;
	virtual bool isEmpty() const = 0;
	virtual void printTree(std::ostream & out = cout) const = 0;		
	virtual void makeEmpty() = 0;
	virtual void insert(const Comparable & item) = 0;
	virtual void remove(const Comparable & item) = 0;
		
		
		
 
};