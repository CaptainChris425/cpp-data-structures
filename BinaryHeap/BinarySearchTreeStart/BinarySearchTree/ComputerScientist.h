#pragma once
#include <string>

using namespace std;

enum SortBy { firstName, lastName, speciality, id };

class ComputerScientist
{
private:
	string firstName;
	string lastName;
	string speciality;
	int id;

public:
	static SortBy sortBy;
	string getFirstName();
	string getLastName();
	string getSpeciality();
	int getID();
	friend ostream& operator<< (ostream &out,const ComputerScientist &computerScientist);
	bool operator==(const  ComputerScientist& rhs) const;
	bool operator<(const ComputerScientist& rhs) const;
	ComputerScientist(const ComputerScientist& rhs);
	ComputerScientist(string firstName, string lastName, string speciality, int id);
	ComputerScientist(string firstName, string lastName);
	ComputerScientist(string speciality);
	ComputerScientist(int id);
	ComputerScientist();
	~ComputerScientist();
};

