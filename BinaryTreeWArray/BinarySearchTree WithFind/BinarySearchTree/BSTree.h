#include "BSTInterface.h"

template <typename Comparable>
class BinarySearchTree : BSTInterface < Comparable >
{
private:
	class BinaryNode
	{
	public:
		Comparable element;
		BinaryNode *left;
		BinaryNode *right;
		//Initialize class members from constructor arguments 
		//by using a member initializer list.
		//This method uses direct initialization, which is more
		//efficient than using assignment operators inside the constructor body.
		BinaryNode(const Comparable & theElement, BinaryNode *left = NULL, BinaryNode *right = NULL)
			: element{ theElement }, left{ left }, right{ right }
		{

		}

	};
	BinaryNode *root = NULL;
	// find the item or the spot it should be inserted
	BinaryNode **  find(const Comparable & item, BinaryNode ** t) const
	{
		if ( *t == NULL || item == (*t)->element)
		{
			return t;

		}
		if (item < (*t)->element)
		{
			return find(item, &((*t)->left));
		}
		else
		{
			return find(item, &((*t)->right));
		}

	}
	
	void remove(const Comparable & item, BinaryNode ** t)
	{
		BinaryNode ** found = find(item, t);
		// item not found exit
		// this will ignore the delete
		if (*found == NULL)
		{
			//nothing to remove
			return;
		}
		//two children
		if ((*found)->left != NULL && (*found)->right != NULL)
		{	
			//overwrite the contents of the item
			//being removed
			(*found)->element = findMax((*found)->left)->element;
			remove((*found)->element, &(*found)->left);

		}//one or no children
		else
		{
			BinaryNode * temp = (*found);
			*found = (*found)->left == NULL ? (*found)->right : (*found)->left;
			delete (temp);
				
		}
	}
	BinaryNode * findMin(BinaryNode *t) const
	{
		//base case 
		if (t->left == NULL)
		{
			return t;
		}
		//recursive call
		return findMin(t->left);
	}
	BinaryNode * findMax(BinaryNode *t) const
	{
		//base case 
		if (t->right == NULL)
		{
			return t;
		}
		//recursive call
		return findMin(t->right);
	}
	void makeEmpty(BinaryNode * & t)
	{
		//post order traversal to delete all nodes
		if (t != NULL)
		{
			makeEmpty(t->left);
			makeEmpty(t->right);
			delete t;
			
		}
	}
	void printTree(BinaryNode *t, std::ostream & out) const
	{
		if (t == NULL)
		{
			return;
		}
		printTree(t->left, out);
		cout << t->element << std::endl;
		printTree(t->right, out);
		
	}
	
public:
	BinarySearchTree()
	{

	};
	
	~BinarySearchTree()
	{
	
	};

	const Comparable & findMin() const
	{
		BinaryNode * max = findMin(root);
		if (max)
		{
			return max->element;
		}
		return Comparable();
	};
	const Comparable & findMax() const
	{
		BinaryNode * min = findMax(root);
		if (min)
		{
			return min->element;
		}
		return Comparable();
	};
		
	const Comparable & find(const Comparable & item) const
	{
		BinaryNode ** found = find(item, &(const_cast<BinaryNode *>(root)));
		if (*found != NULL)
		{
			return (*found)->element;
		}
		//throw exeception
		return Comparable();
	};

	bool contains(const Comparable & item) const
	{
		BinaryNode ** found = find(item, &(const_cast<BinaryNode *>(root)));
		return *found != NULL;
	};
	bool isEmpty() const{

		return root == NULL;

	};
	void printTree(std::ostream & out = cout) const
	{
		printTree(root, out);
	};

	void makeEmpty()
	{
		makeEmpty(root);
		root = NULL;
	};
	void insert(const Comparable & item)
	{
		BinaryNode ** found = find(item, &root);
		//insert a new item if there is no item with this key
		if (*found == NULL)
		{
			*found = new BinaryNode(item);

		}
	};
	
	void remove(const Comparable & item)
	{
		remove(item, &root);
	};

	


};