// BinarySearchTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BSTree.h"
#include "ComputerScientist.h"

int _tmain(int argc, _TCHAR* argv[])
{
	ComputerScientist::sortBy = SortBy::id;
	BinarySearchTree<ComputerScientist> tree;
	tree.insert(ComputerScientist("Robert", "Ward", "Education", 50));
	tree.insert(ComputerScientist("Grace", "Hopper", "Compilers", 40));
	tree.insert(ComputerScientist("Alan", "Turing", "Cryptogarphy", 60));
	tree.insert(ComputerScientist("John", "von Neumann", "Hardware", 45));
	tree.insert(ComputerScientist("Dennis", "Ritchie", "Compilers", 55));
	tree.insert(ComputerScientist("Ada", "Lovelace", "Programming", 30));
	tree.insert(ComputerScientist("Edsger", "Dikstra", "Algorithms", 80));
	tree.insert(ComputerScientist("Ken", "Thompson", "OS", 70));

	tree.printTree();
	ComputerScientist find(80);
		
	ComputerScientist find2 = tree.find(find);

	cout << endl;

	cout << "remove one child node "<< endl;
	//one child
	ComputerScientist removeOneChild(80);
	tree.remove(removeOneChild);
	tree.printTree();
	//no child
	cout << "remove no child node " << endl;
	ComputerScientist removeNoChild(70);
	tree.remove(removeNoChild);
	tree.printTree();
	//two child
	cout << "remove two child node " << endl;
	ComputerScientist removeTwoChild(50);
	tree.remove(removeTwoChild);

	tree.printTree();

		
	system("pause");
	return 0;
}

