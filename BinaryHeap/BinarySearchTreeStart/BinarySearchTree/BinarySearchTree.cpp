// BinarySearchTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BSTree.h"
#include "ComputerScientist.h"
#include <cstdlib>
#include <cmath>

int _tmain()
{
	ComputerScientist::sortBy = SortBy::id;
	BinarySearchTree<int> tree;
	//BinarySearchTree<ComputerScientist> tree;
	tree.insert(1);
	tree.insert(2);
	tree.insert(17);
	tree.insert(25);
	tree.insert(4);
	tree.insert(6);
	tree.insert(9);
	/*tree.insert(ComputerScientist("Robert", "Ward", "Education", 50));
	tree.insert(ComputerScientist("Grace", "Hopper", "Compilers", 40));
	tree.insert(ComputerScientist("Alan", "Turing", "Cryptogarphy", 60));
	tree.insert(ComputerScientist("John", "von Neumann", "Hardware", 45));
	tree.insert(ComputerScientist("Dennis", "Ritchie", "Compilers", 55));
	tree.insert(ComputerScientist("Ada", "Lovelace", "Programming", 30));
	tree.insert(ComputerScientist("Edsger", "Dikstra", "Algorithms", 80));
	tree.insert(ComputerScientist("Ken", "Thompson", "OS", 70));*/

	tree.printTree();
	//ComputerScientist find(80);
		
	//ComputerScientist find2 = tree.find(find);
		
	system("pause");
	return 0;
}

