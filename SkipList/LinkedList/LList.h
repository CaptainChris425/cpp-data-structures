//#include <assert.h>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <vector>

#include "ListADT.h"

using namespace std;
// Linked list implementation
template <typename E> class LList  : ListADT<typename E> {
private:
	//singly linked list node
	template <typename E> class Node
	{
	public:
		E element; // Value for this node
		Node * next; //pointer to the next node in the list
		Node * previous; //pointer to the next node in the list
		Node * down;
	
		Node(const E& elemval, Node* next = nullptr, Node* previous = nullptr, Node* down = nullptr)
		{
			element = elemval;
			this->next = next;
			this->previous = previous;
			this->down = down;
		}
		Node(Node* next = nullptr, Node* previous = nullptr, Node* down = nullptr)
		{
			this->next = next;
			this->previous = previous;
			this->down = down;
		}
		~Node(){};
	};
	vector < Node<E>*> skipList;
	Node<E>* head; // Pointer to list header
	Node<E>* tail; // Pointer to last element
	Node<E>* curr; // Access to current element
	Node<E>* below;
	int size = 0; // Size of vector

	// Intialization helper method
	// set up list to be used
	void init()
	{ 
		srand(time(0));
		head = nullptr;
		tail = nullptr;
		below = nullptr;
		curr = head;
		size = 0;
	}

	void insertintolist(int height, const E& item) {

		

			cout << "Inserting into list : " << item << endl;
			if (height > size) {
				return;
			}
			
			if (skipList[height] == nullptr) {
				skipList[height] = new Node<E>(item);
				curr = skipList[height];
				if (height != 0 && below != nullptr) {
					curr->down = below;
					below = curr;
				}
				else {
					below = curr;
				}
			}else if (item < skipList[height]->element && skipList[height] != nullptr) {
				E tempvar = skipList[height]->element;
				skipList[height]->element = item;
				if (below == nullptr || below->element != item) {
					below = skipList[height];
				}
				else {
					skipList[height]->down = below;
					below = skipList[height];
				}
				insertintolist(height, tempvar);

			}
			else {
				head = skipList[height];
				curr = skipList[height];
				Node<E>* temp = curr;
				//
				//while item is greater than current and currents next is not null
				//move the current and temp one over
				//
				while (curr->next != nullptr && curr->element < item) {
					cout << curr->element << " is less than (moving) " << item << endl;
					curr = temp;
					temp = temp->next;
				}
				//
				//if item less than the curr and curr previous isnt nullptr
				//insert the new node behind current
				//vvvvvvvvvvvvvvv
				if (curr->element > item && curr->previous != nullptr) {
					temp = curr->previous;
					temp->next = new Node<E>(item);
					temp->next->previous = temp;
					curr->previous = temp->next;
					temp->next->next = curr;
					//
					if (below->element != item) {
						below = temp->next;
					}
					else if (below != nullptr) {
						temp = temp->next;
						temp->down = below;
						temp = curr->previous->previous;
					}
					//

				}
				
				else if (curr->element < item && curr->next == nullptr) {
					curr->next = new Node<E>(item);
					curr->next->previous = curr;
					// 
					if (below->element != item) {
						below = curr->next;
					}
					else if (below != nullptr) {
						temp = curr->next;
						temp->down = below;
						temp = curr;
						below = curr->next;
					}
					
					// 
				}

			}
		//}
	}

	void removep(const E& item, int height = 0) {
		height = getnodeheight(item);

		while (height >= 0) {
			Node<E>* temp = skipList[height];
			if (skipList[height] != nullptr && temp->next == nullptr) {
				skipList[height] = nullptr;
				size--;
			}
			while (skipList[height] != nullptr && temp->next != nullptr) {
				if (skipList[height]->element == item) {
					skipList[height] = temp->next;
					temp->next->previous = nullptr;
					temp->next = nullptr;
				}
				else if (temp->element == item) {
					Node<E>* temp2 = temp->previous;
					temp2->next = temp->next;
					temp->next->previous = temp2;
					temp->next = nullptr;
				}
				else if (temp->next->element == item && temp->next->next == nullptr) {
					temp = temp->next;
					temp->previous->next = nullptr;
				}
				else {
					temp = temp->next;
				}
			}
			delete temp;
			height--;
		}
		
	}

	Node<E>* findp(const E& item, int height = size)const {
		if (height == 0) {
			return nullptr;
		}
		Node<E>* temp = skipList[height];
		

		while (temp->element != item) {
			while (temp->element < item && temp->next != nullptr) {
				temp = temp->next;
			}
			if (temp->element > item && temp->previous != nullptr && temp->down != nullptr) {
				if (temp->previous->down == nullptr) {
					cout << endl << "temp == nullptr" << endl;
				}
				temp = temp->previous->down;
				height--;
			}
			else if (temp->previous == nullptr && temp->element > item && temp->down != nullptr) {
				if (height > 0) {
					height--;
					temp = skipList[height];
				}
				else { return nullptr; }
			}
			else if (item > temp->element && temp->previous == nullptr  && temp->down != nullptr) {
				if (height > 0) {
					height--;
					temp = skipList[height];
				}
				else { return nullptr; }
			}
			if (temp->next == nullptr && temp->down == nullptr) {
				return nullptr;
			}
			else if (temp->element > item && temp->previous == nullptr && temp->down == nullptr) {
				return nullptr;
			}
			if (temp->previous != nullptr && temp->previous->element < item && temp->element > item && temp->down == nullptr) {
				return nullptr;
			}
			
		}
		return temp;
	}

	void printp(int height, std::ostream &out = cout) const{
		if (height > -1) {
			if (skipList[height] != nullptr) {
				Node<E>* temp = skipList[height];
				while (temp != nullptr) {
					cout << temp->element << " , ";
					temp = temp->next;
				}
				cout << endl;
				printp(height - 1, out);
			}
		}
	}
	
	int getnodeheight(const E& item, int height = 0) {
		height = size;
		Node<E>* temp = skipList[height];
		while (height >= 0) {
			temp = skipList[height];
			while (skipList[height] != nullptr && temp->next != nullptr) {
				if (temp->element == item) {
					return height;
				}
				temp = temp->next;
			}
			if (temp->element == item) {
				return height;
			}
			height--;
		}
	}

	void removeall() 
	{   
	
	}
	
public:
	// Constructor
	LList()
	{
		init(); 
	}
	// Destructor
	~LList()
	{	
		
	} 

	bool find(const E& item) {
		if (findp(item, size) == nullptr) {
			return false;
		}
		else {
			return true;
		}
	}

	/**
		calls the private method with
		the size of the height vector
	*/
	void print(std::ostream &out = cout) const
	{   
		int temp = size;
		printp(temp, out);
	}
	/**
	*   clears elements from the lists
	*/
	void clear() 
	{ 

	}
	/**
		insert an item into the skiplist
	*/
	void insert(const E& item) 
	{
		int height = 0;
		int flip = (rand() % 2);

		while (flip == 1) {
			height += 1;
			flip = (rand() % 2);
		}
		if (height > size) {
			size = height;
		}
		while (skipList.size() < height) {
			skipList.push_back(nullptr);
		}
		if (skipList.size() == 0) {
			skipList.push_back(nullptr);
		}
		if (height == 0) {
			insertintolist(0, item);
		}
		else {
			for (int i = 0; i <= height-1; i++) {
				insertintolist(i, item);
			}
		}
		size = skipList.size() - 1;
		head = skipList[0];

	}

	void remove(const E& item) {
		removep(item, 0);
	}


	/**
	*   @brief remove and return the current Element
	*
	*   @return E the type stored in this List
	*/
	E remove()
	{		
		return E();
	}
	/**
	Moves current to start of list;
	*/
	void moveToStart() 
	{
		if (curr != head) {
			curr = head;
			return;
		}
		return;
		
	}
	/**
	moves current to end of list
	*/
	void moveToEnd() 
	{
		if (curr != tail) {
			curr = tail;
			return;
		}
		return;
		
	}

	/**
	*   @brief  Get the current number of items in the list.
	*
	*
	*   @return int
	*/
	int length() const 
	{ 
		return size;
	} 
	

	/**
	*   @brief  Return the current Element
	*
	*
	*   @return E the type of this List
	*/
	const E& getValue() const 
	{ 
		return E();
	}
};
