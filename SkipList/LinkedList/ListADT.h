#pragma once
template <typename E> class ListADT
{

private:

	void operator =(const ListADT&) {} // protect assignment
	ListADT(const ListADT&) {} //protect copy constructor
public:

	ListADT() { }; //Default Constructor

	virtual ~ListADT() {}; //Base Destructor

	/**
	*   @brief clear all elements from the list.
	*
	*   @param  E The item to insert.
	*   @return void
	*/
	virtual void clear() = 0;

	
	/**
	*   @brief insert an Element before the current location
	*
	*   @param  E The item to insert.
	*   @return void
	*/
	virtual void insert(const E& item) = 0;

	
	/**
	*   @brief remove and return the current Element
	*
	*   @return E the type stored in this List
	*/
	virtual E remove() = 0;

	/**
	*   @brief Move the current position to the beginning of the list.
	*   Note: no change if already at the end.
	*
	*   @return void
	*/
	virtual void moveToStart() = 0;
	/**
	*   @brief Move the current position to the end of the list.
	*   Note: no change if already at the end.
	*
	*   @return void
	*/
	virtual void moveToEnd() = 0;


	/**
	*   @brief Returns true if current position is not at the end of the list.
	*
	*   @return bool
	*/
	
	virtual int length() const = 0;

	

	
	/**
	*   @brief  Set the current position.
	*   The postion is zero based    
	*   
	*   @param  pos The position to make current.  
	*   @return void
	*/

	virtual const E& getValue() const = 0;


};

